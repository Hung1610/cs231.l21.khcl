import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing import image
#VGG16
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input
#VGG19
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.applications.vgg19 import preprocess_input

#models
from keras.models import Sequential
from keras.layers.core import Flatten


def build_model(desired_model):
    vgg_conv = None
    model = Sequential()

    if desired_model == 'vgg_19':
        vgg_conv = VGG19(weights='imagenet', include_top=False, input_shape=(64, 64, 3))

    if desired_model == 'vgg_16':
        vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=(64, 64, 3))

    for layer in vgg_conv.layers:
        model.add(layer)

    model.add(Flatten())
    
    return model


def image_to_vect(model, data_dir):
    img = image.load_img(data_dir, target_size=(64, 64))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    features_vect = model.predict(x)

    return features_vect


def preprocess_image(model, data_dir, saving_dir):
    features_vect = image_to_vect(model, data_dir)
    save_vector = np.array(features_vect)
    np.save(saving_dir, save_vector[0])

    return save_vector


def feature_extract(args):
    tf.config.threading.set_intra_op_parallelism_threads(args.no_cpu)
    tf.config.threading.set_inter_op_parallelism_threads(args.no_cpu)

    model = build_model(args.model)

    saved_feature_path = f'''{args.save_feature_dir}/{args.model}'''
    if not os.path.exists(saved_feature_path):
        os.mkdir(saved_feature_path)

    classes = os.listdir(args.train_dir)
    class_order = 0
    for class_object in classes:
        counter = 0
        save_vector = None
        if class_object == '.DS_Store':
            continue

        class_dir = f'''{args.train_dir}/{class_object}/'''
        features_class_dir = f'''{args.save_feature_dir}/{args.model}/{class_object}/'''
        datas = os.listdir(class_dir)

        if not os.path.exists(features_class_dir):
            os.mkdir(features_class_dir)

        for data in datas:
            if data == '.DS_Store':
                continue

            data_dir = class_dir + data
            file_info = data.split('.')
            saving_dir = f'''{features_class_dir}/{file_info[0]}'''
            save_vector = preprocess_image(model, data_dir, saving_dir)

            counter +=1
        class_order += 1
        print(f'''{class_order}. Done {args.save_feature_dir}/{args.model}/{class_object} last vect: {save_vector.shape} number of file: {counter}''')
    
    return 'nope'


def test_set_feature_extract(args):
    tf.config.threading.set_intra_op_parallelism_threads(args.no_cpu)
    tf.config.threading.set_inter_op_parallelism_threads(args.no_cpu)

    test_img_files = os.listdir(args.test_dir)
    
    model = build_model(args.model)

    if not os.path.exists(args.save_test_dir):
        os.mkdir(args.save_test_dir)

    for file_object in test_img_files:
        input_file = f'''{args.test_dir}/{file_object}'''
        file_info = file_object.split('.')
        saving_file = f'''{args.save_test_dir}/{args.model}/{file_info[0]}'''

        if not os.path.exists(f'''{args.save_test_dir}/{args.model}'''):
            os.mkdir(f'''{args.save_test_dir}/{args.model}''')

        save_vector = preprocess_image(model, input_file, saving_file)

        print(f'''savin at: {saving_file} shape: {save_vector.shape}''')