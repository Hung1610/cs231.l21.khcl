import os
import pickle
import numpy as np

# train
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import BernoulliNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split

# validate func
from sklearn.metrics import accuracy_score

from .constant import VALID_MODELS, PREPROCESS_MODEL, CLASS_TEXT_TO_NUM

class MachineLearning():

    checkpoint_path = None
    model = None
    X_train = None
    X_test = None
    Y_train = None
    Y_test = None
    is_train = False

    def __init__(self, checkpoint_dir, preprocess_model, learning_model, is_train=False):
        if learning_model not in VALID_MODELS:
            print(f'''learning_model: NOT A VALID MODEL, MODEL HAVE TO BE {VALID_MODELS}''')
            exit(0)
        
        if preprocess_model not in PREPROCESS_MODEL:
            print(f'''preprocess_model: NOT A VALID MODEL, MODEL HAVE TO BE {PREPROCESS_MODEL}''')
            exit(0)

        self.checkpoint_path = f'''{checkpoint_dir}/feature_{preprocess_model}_finalized_model_{learning_model}.sav'''
        self.is_train = is_train

        if not os.path.exists(self.checkpoint_path) or self.is_train:
            self.model = self.model_selector(learning_model)
            print(f'''successfully init model: {self.model}''')

        if os.path.exists(self.checkpoint_path) and not self.is_train:
            self.model = self.load_model_from_checkpoint(self.checkpoint_path)
            print(f'''successfully loaded model: {self.model}''')

    
    def train(self):
        if os.path.exists(self.checkpoint_path) and not self.is_train:
            print(f'''cannot continue to train loaded model''')
            return

        print(f'''Trained by model: {self.model}''')
        self.model.fit(self.X_train, self.Y_train)
        print(f'''Done train {self.model}''')
    
        pickle.dump(self.model, open(self.checkpoint_path, 'wb'))
        print(f'''model saved at: {self.checkpoint_path}''')

        return


    def evaluate_model(self):
        clf = self.model
        print(f'''proceed predict''')
        test_prediction=self.predict(self.X_test)
        
        print(f'''Done test''')
        print(f'''proceed scorin''')
        train_arc = clf.score(self.X_train, self.Y_train)
        test_arc = accuracy_score(self.Y_test, test_prediction)

        print(f'''accuracy on train: {train_arc*100} | accuracy on test: {test_arc*100}''')
    
        return


    def predict(self, input_X, labels=None):
        prediction = self.model.predict(input_X)

        if labels is not None:
            test_arc = accuracy_score(labels, prediction)
            print(f'''accuracy on current set: {test_arc*100}''')

        return prediction


    def split_and_save_data(self, X, Y):
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(X, Y, test_size=0.2, random_state=2020)

        return self.X_train, self.X_test, self.Y_train, self.Y_test


    def data_loader(self, feature_dir, preprocess_model):
        '''
        input:
            feature_dir: path to directory where .npy vector is saved
            preprocess_model: vgg_16 or vgg_19
        output:
            X: Vector
            Y: Label
        '''
        X = []
        Y = []

        data_dir = f'''{feature_dir}/{preprocess_model}'''

        classes = os.listdir(data_dir)

        for class_object in classes:
            if class_object == '.DS_Store':
                continue

            class_dir = f'''{data_dir}/{class_object}/'''
            datas = os.listdir(class_dir)
            
            for data in datas:

                if data == '.DS_Store':
                    continue

                data_dir = class_dir + data
                file_info = data.split('.')

                if file_info[1] != 'npy':
                    print('feature need to be a numpy file')
                    exit(0)

                feature = np.load(data_dir) 
                label = CLASS_TEXT_TO_NUM[class_object]

                X.append(np.asarray(feature))
                Y.append(label)

            print(f'''Done import feature data at {feature_dir}/{class_object}''')

        return np.asarray(X), np.asarray(Y)


    def model_selector(self, learning_model):
        '''
        input:
            learning_model: ['BernoulliNB', 'DecisionTreeClassifier', 'SVC', 'SVC(OvO)', 'LinearSVC']
        output:
            model object
        '''
        return_model = None
    
        if learning_model == 'BernoulliNB':
            return_model = BernoulliNB()

        if learning_model == 'DecisionTreeClassifier':
            return_model = DecisionTreeClassifier()

        if learning_model == 'SVC':
            return_model = SVC()

        if learning_model == 'SVC(OvO)':
            return_model = SVC(decision_function_shape='ovo')

        if learning_model == 'LinearSVC':
            return_model = LinearSVC()

        return return_model


    def load_model_from_checkpoint(self, checkpoint_path):
        print('-'*100)
        print(f'''found checkpoint loading model from {checkpoint_path}''')
        loaded_model = pickle.load(open(checkpoint_path, 'rb'))
        print('-'*100)

        return loaded_model