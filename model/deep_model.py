import os
import numpy as np
import tensorflow as tf

from tensorflow.keras import callbacks
from tensorflow.keras.models import Sequential

#VGG16
from tensorflow.keras.applications.vgg16 import VGG16
#VGG19
from tensorflow.keras.applications.vgg19 import VGG19

from keras.layers.core import Flatten, Dense
from tensorflow.keras.optimizers import Adam

from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from sklearn.utils import shuffle

from .constant import CLASS_TEXT_TO_NUM

class DeepModel():
    model = None
    preprocess_model = None
    checkpoint_path = None
    keras_call_backs = []

    classes = 29
    batch_size = 64
    epochs = 5
    lnr = 0.0001

    train = None
    val = None

    def __init__(self, pretrain_dir, preprocess_model='vgg_19', first_run=True):
        self.model = Sequential()
        self.preprocess_model = preprocess_model

        self.build_model()
        
        if not first_run and os.path.exists(pretrain_dir):
            latest = tf.train.latest_checkpoint(pretrain_dir)
            print(f'''found model at {pretrain_dir}/{latest}''')
            self.model.load_weights(latest)
            print(f'''restored model {latest}''')
        
        self.model.summary()

        pass


    def train(self, X, Y):
        X, Y = shuffle(X, Y, random_state=0)
        generator = ImageDataGenerator(rescale=1./255, validation_split=0.1)
        self.train = generator.flow(X, Y, batch_size=self.batch_size, subset ='training')
        self.val = generator.flow(X, Y, batch_size=self.batch_size, subset ='validation')

        self.get_call_back()

        self.model.save_weights(self.checkpoint_path.format(feature=self.preprocess_model, epoch=0))

        self.model.fit(
            self.train,
            validation_data = self.val,
            epochs=self.epochs,
            shuffle=True,
            batch_size=self.batch_size,
            verbose=1, 
            callbacks = self.keras_call_backs
        )


    def predict(self, X):
        test = ImageDataGenerator(rescale=1./255).flow(X, batch_size=self.batch_size)

        pred = self.model.predict(test)
        pred = np.argmax(pred, axis=1)

        return pred


    def build_model(self):
        adam = Adam(learning_rate=self.lnr)

        if self.preprocess_model == 'vgg_16':
            self.model.add(VGG16(weights='imagenet', include_top=False, input_shape=(64, 64, 3)))
        if self.preprocess_model == 'vgg_19':
            self.model.add(VGG19(weights='imagenet', include_top=False, input_shape=(64, 64, 3)))

        self.model.add(Flatten())
        self.model.add(Dense(256, activation='relu'))
        self.model.add(Dense(29, activation='softmax'))

        self.model.compile(optimizer=adam, loss='sparse_categorical_crossentropy', metrics=['accuracy'] )
        
        return


    def get_call_back(self):
        self.checkpoint_path = '''./checkpoints/DL/CS-{epoch:04d}.ckpt'''
        os.path.dirname(self.checkpoint_path)

        # Create a callback that saves the model's weights every epochs
        cp_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=self.checkpoint_path, 
            verbose=1, 
            save_weights_only=True
        )

        earlystopping = callbacks.EarlyStopping(
            monitor="val_loss",
            mode="min", 
            patience=5,
            restore_best_weights = True
        )

        self.keras_call_backs = [earlystopping, cp_callback]



    def data_loader(self, image_dir):
        '''
        input:
            image_dir: path to directory where .jpg is at
            preprocess_model: vgg_16 or vgg_19
        output:
            X: Vector
            Y: Label
        '''
        X = []
        Y = []

        data_dir = f'''{image_dir}'''

        classes = os.listdir(data_dir)

        for class_object in classes:
            if class_object == '.DS_Store':
                continue

            class_dir = f'''{data_dir}/{class_object}'''
            image_files = os.listdir(class_dir)
            
            for image_file in image_files:
                if image_file == '.DS_Store':
                    continue

                imd_dir_to_file = f'''{class_dir}/{image_file}'''
                file_info = image_file.split('.')

                if file_info[1] != 'jpg':
                    print('feature need to be a jpg file')
                    exit(0)

                img = image.load_img(imd_dir_to_file, target_size=(64, 64))
                img_data = image.img_to_array(img) 
                label = CLASS_TEXT_TO_NUM[class_object]

                X.append(img_data)
                Y.append(label - 1)

            print(f'''Done import feature data at {image_dir}/{class_object}''')

        return np.asarray(X), np.asarray(Y)