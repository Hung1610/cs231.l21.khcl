import os
import argparse as arg
import numpy as np

from data_preprocess.feature_extract import feature_extract, test_set_feature_extract

def preprocessor(args):
    if not os.path.isdir(args.save_feature_dir):
        os.mkdir(args.save_feature_dir)

    feature_extract(args)

    if not os.path.isdir(args.save_test_dir):
        os.mkdir(args.save_test_dir)

    test_set_feature_extract(args)


def main():
    parser = arg.ArgumentParser(description='preprocessor for data set')
    parser.add_argument('--model', default='vgg_16', help='enter your desired model or it will automaticly chose vgg_!6')
    parser.add_argument('--save_feature_dir', default='./features', help='where to store the train extracted feature vector')
    parser.add_argument('--save_test_dir', default='./test', help='where to store the test extracted feature vector')
    parser.add_argument('--train_dir', default='./dataset/archive/asl_alphabet_train/asl_alphabet_train', help='where to store the test extracted feature vector')
    parser.add_argument('--test_dir', default='./dataset/archive/asl_alphabet_test/asl_alphabet_test', help='where to store the test extracted feature vector')
    parser.add_argument('--no_cpu', type=int, default=1, help='where to store the test extracted feature vector')
    args = parser.parse_args()
    preprocessor(args)

if __name__ == '__main__':
    main()