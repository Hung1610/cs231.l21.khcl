import argparse as arg

from model.deep_model import DeepModel
from model.ml_model import MachineLearning

def train_deep(args):
    model = DeepModel(preprocess_model=args.preprocess_model)

    X, Y = model.data_loader(args.train_image_dir)
    model.train(X, Y)

    return

def train_supervise(args):
    predict_model = MachineLearning(
        checkpoint_dir=args.checkpoint_dir,
        preprocess_model=args.preprocess_model,
        learning_model=args.learning_model,
        is_train=True
    )

    X, Y = predict_model.data_loader(args.train_image_feature_dir, args.preprocess_model)
    predict_model.split_and_save_data(X, Y)
    predict_model.train()
    predict_model.evaluate_model()


def main():
    parser = arg.ArgumentParser(description='preprocessor for data set')
    parser.add_argument('--preprocess_model', default='vgg_16', help='enter your desired model or it will automaticly chose vgg_16 as default')
    parser.add_argument('--learning_model', default='SVC', help='enter your desired model or it will automaticly chose SVC as default')
    parser.add_argument('--checkpoint_dir', default='./checkpoints', help='where to store the train extracted feature vector')
    parser.add_argument('--checkpoint_priority', type=bool, default=True, help='true if you want to train model from scratch instead of using existed model')
    parser.add_argument('--train_image_dir', default='./dataset/archive/asl_alphabet_train/asl_alphabet_train', help='true if you want to train model from scratch instead of using existed model')
    parser.add_argument('--train_image_feature_dir', default='./feature', help='true if you want to train model from scratch instead of using existed model')
    args = parser.parse_args()

    if args.learning_model == 'deep_learning':
        train_deep(args)
    
    if args.learning_model != 'deep_learning':
        train_supervise(args)

if __name__ == '__main__':
    main()