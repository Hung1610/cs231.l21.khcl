import os
import argparse as arg
import numpy as np
import pandas as pd

from model.ml_model import MachineLearning
from model.deep_model import DeepModel
from model.constant import CLASS_TEXT_TO_NUM, CLASS_ARR

from data_preprocess.feature_extract import build_model, image_to_vect

from tensorflow.keras.preprocessing import image

from sklearn.metrics import accuracy_score


def build_and_run(args):
    truth_label = []
    prediction_label = []
    predict_model = MachineLearning(
        checkpoint_dir=args.checkpoint_dir,
        preprocess_model=args.preprocess_model,
        learning_model=args.learning_model
    )

    image_list = os.listdir(args.image_dir)

    preprocess_model = build_model(args.preprocess_model)
    for image_file in image_list:
        file_information = image_file.split('_')
        if file_information[1] != 'test.jpg':
            print(f'''{image_file} is not a test image, a test image need to be a <class>_test.jpg''')
            exit(0)

        features_vect = image_to_vect(preprocess_model, f'''{args.image_dir}/{image_file}''')
        label = CLASS_TEXT_TO_NUM[file_information[0]]

        predict_Y = predict_model.predict([features_vect[0]])

        print(f'''{args.image_dir}/{image_file}''')
        print(f'''image true label: {file_information[0]} | predicted label: {CLASS_ARR[predict_Y[0] - 1]}''')

        truth_label.append(label)
        prediction_label.append(predict_Y[0])
    
    arc = accuracy_score(truth_label, prediction_label)
    print(f'''arccuracy on test: {arc*100}''')
    return


def build_and_run_deep_model(args):
    truth_label = []
    prediction_label = []
    predict_model = DeepModel(
        pretrain_dir=args.checkpoint_dir,
        preprocess_model=args.preprocess_model,
        first_run=False
    )

    image_list = os.listdir(args.image_dir)

    for image_file in image_list:
        file_information = image_file.split('_')
        if file_information[1] != 'test.jpg':
            print(f'''{image_file} is not a test image, a test image need to be a <class>_test.jpg''')
            exit(0)

        img = image.load_img(f'''{args.image_dir}/{image_file}''', target_size=(64, 64))
        img_data = image.img_to_array(img) 
        label = CLASS_TEXT_TO_NUM[file_information[0]]

        predict_Y = predict_model.predict(np.asarray([img_data]))

        print(f'''{args.image_dir}/{image_file}''')
        print(f'''image true label: {file_information[0]} | predicted label: {CLASS_ARR[predict_Y[0]]}''')

        truth_label.append(label - 1)
        prediction_label.append(predict_Y[0])

    arc = accuracy_score(truth_label, prediction_label)
    print(f'''arccuracy on test: {arc*100}''')
    return


def main():
    parser = arg.ArgumentParser(description='preprocessor for data set')
    parser.add_argument('--preprocess_model', default='vgg_16', help='enter your desired model or it will automaticly chose vgg_16 as default')
    parser.add_argument('--learning_model', default='SVC', help='enter your desired model or it will automaticly chose SVC as default')
    parser.add_argument('--checkpoint_dir', default='./checkpoints', help='where to store the train extracted feature vector')
    parser.add_argument('--checkpoint_priority', type=bool, default=True, help='true if you want to train model from scratch instead of using existed model')
    parser.add_argument('--image_dir', default='./dataset/archive/asl_alphabet_test/asl_alphabet_test', help='true if you want to train model from scratch instead of using existed model')
    args = parser.parse_args()

    if args.learning_model == 'deep_learning':
        build_and_run_deep_model(args)

    if args.learning_model != 'deep_learning':
        build_and_run(args)

if __name__ == '__main__':
    main()